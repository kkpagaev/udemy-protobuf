package main

import (
	"fmt"
	"log"

	pb "gitlab.com/kkpagaev/udemy-protobuf/proto"
	"google.golang.org/protobuf/proto"
)

func main() {
	user := &pb.User{
		Name: "kkpagaev",
		Email:    "test@test.com",
		Password: "123456",
		Role:     []pb.User_Roles{pb.User_USER, pb.User_ADMIN, pb.User_MODERATOR, pb.User_INVITATOR},
	}
	out, err := proto.Marshal(user)
	if err != nil {
		log.Fatalln("Can't serialise to bytes", err)
		return 
	}
	newUser := &pb.User{}
	err = proto.Unmarshal(out, newUser)
	if err != nil {
		log.Fatalln("Can't deserialise to bytes", err)
		return
	}
	newUser.Email = "kapagaev@gmail.com"
	fmt.Println("user")
	fmt.Println(user)
	fmt.Println("new user")
	fmt.Println(newUser)
}
